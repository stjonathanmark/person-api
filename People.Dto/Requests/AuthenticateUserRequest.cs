﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.AuthenticateUser)]
    public class AuthenticateUserRequest : BaseRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}