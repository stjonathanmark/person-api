﻿namespace People
{
    public enum PasswordChangeStatus
    {
        NotChanged,
        InvalidCurrentPassword,
        InvalidFormat,
        Reused,
        Error,
        Changed
    }
}
