﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using People.Data;
using People.Dto;
using People.Security;
using People.Security.Authentication;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace People.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {

        private readonly IPasswordHasher pwdHasher;
        private readonly ISecuritySettings security;
        private readonly IMapper mapper;

        public AccountController(ISecuritySettings securitySettings, IPasswordHasher passwordHasher, DataContext context, IMapper mapper)
            : base(context)
        {
            security = securitySettings;
            pwdHasher = passwordHasher;
            this.mapper = mapper;
        }

        // Body Properties
        // 1) username: [string] - Value for user's username - required
        // 2) password: [string] - Value for user's password - required
        // 3) roleId: [integer] - Id of role to be assigned to user - required
        // 4) firstName: [string] - Value for user's first name - required
        // 5) lastName: [string] - Value for user's last name - required
        // 6) email: [string] - Value for user's name - required
        [HttpPost]
        public async Task<CreateAccountResponse> CreateAccount(CreateAccountRequest request)
        {
            var response = new CreateAccountResponse() { Status = UserCreationStatus.NotCreated };

            if (security.EnforcePasswordFormat && !security.UsernameRegex.IsMatch(request.Username)) response.Status = UserCreationStatus.InvalidUsernameFormat;

            else if (db.Users.Any(u => u.Username == request.Username)) response.Status = UserCreationStatus.UsernameExists;

            else if (security.EnforcePasswordFormat && !security.PasswordRegex.IsMatch(request.Password)) response.Status = UserCreationStatus.InvalidPasswordFormat;

            else
            {
                var user = new User()
                {
                    Username = request.Username,
                    Password = request.Password,
                    RoleId = request.RoleId,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email
                };

                user.Salt = pwdHasher.GenerateSalt();
                user.Password = pwdHasher.Hash(user.Password, user.Salt);

                user.CreationDate = DateTime.Now;

                await db.Users.AddAsync(user);
                await db.SaveChangesAsync();

                response.Successful = true;
                response.Status = UserCreationStatus.Created;
                response.Message = request.DefaultSuccessMessage;
            }

            if (response.Status != UserCreationStatus.Created) response.Message = request.DefaultErrorMessage;

            return response;
        }

        // Body Properties
        // 1) username: [string] - Value for user's username - required
        // 2) password: [string] - Value for user's password - required
        [Route("authenticate")]
        [HttpPost]
        public async Task<AuthenticateUserResponse> AuthenticateUser(AuthenticateUserRequest request)
        {
            var response = new AuthenticateUserResponse() { Status = AuthenticationStatus.NotAuthenticated };
            var user = await db.Users.Include(u => u.Role).FirstOrDefaultAsync(u => u.Username == request.Username);

            if (user == null) response.Status = AuthenticationStatus.UserDoesNotExist;

            else if (user.Password != pwdHasher.Hash(request.Password, user.Salt)) response.Status = AuthenticationStatus.InvalidPassword;

            else
            {
                if (security.UseTokens) response.Token = GetToken(user);
                response.Successful = true;
                response.Status = AuthenticationStatus.Authenticated;
                response.Message = request.DefaultSuccessMessage;
            }

            if (response.Status != AuthenticationStatus.Authenticated) response.Message = request.DefaultErrorMessage;

            return response;
        }

        // Body Properties
        // 1) username: [string] - User's username - required
        // 2) newPassword: [string] - Value for user's new password - required
        // 2) currentPassword: [string] - Value for user's current password
        [Route("change-password")]
        [HttpPost]
        public async Task<ChangePasswordResponse> ChangePassword(ChangePasswordRequest request)
        {
            var response = new ChangePasswordResponse() { Status = PasswordChangeStatus.NotChanged };

            var user = await db.Users.FirstOrDefaultAsync(u => u.Username == request.Username);

            var newHashedPwd = string.Empty;
            var currPwd = request.CurrentPassword;

            if (!string.IsNullOrEmpty(currPwd) && user.Password != pwdHasher.Hash(currPwd, user.Salt)) response.Status = PasswordChangeStatus.InvalidCurrentPassword;

            else if (security.EnforcePasswordFormat && !security.PasswordRegex.IsMatch(request.NewPassword)) response.Status = PasswordChangeStatus.InvalidFormat;

            else if ((newHashedPwd = pwdHasher.Hash(request.NewPassword, user.Salt)) == user.Password) response.Status = PasswordChangeStatus.Reused;

            else
            {
                if (security.UseSaltedPasswords) user.Salt = pwdHasher.GenerateSalt();
                else if (!string.IsNullOrWhiteSpace(user.Salt)) user.Salt = string.Empty;

                user.Password = pwdHasher.Hash(request.NewPassword, user.Salt);

                await db.SaveChangesAsync();

                response.Successful = true;
                response.Status = PasswordChangeStatus.Changed;
                response.Message = request.DefaultSuccessMessage;
            }

            if (response.Status != PasswordChangeStatus.Changed) response.Message = request.DefaultErrorMessage;

            return response;
        }

        [Route("roles")]
        [HttpGet]
        public async Task<GetRolesResponse> GetRoles([FromQuery] GetRolesRequest request)
        {
            var response = new GetRolesResponse();

            try
            {
                response.Roles = mapper.Map<IEnumerable<RoleDto>>(await SelectAsync<Role>(string.Empty, request.OrderBys, request.Skip, request.Take, request.IncludeDeleted));
                response.PagingInfo = GetPagingInfo(request, ItemsFound);
                response.Successful = true;
                response.Message = request.DefaultSuccessMessage;
            }
            catch(Exception exception)
            {
                HandleException(response, exception, request.DefaultErrorMessage);
            }

            return response;
        }

        private string GetToken(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(security.TokenKey));

            var tokenDesc = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new List<Claim>() {
                    new Claim(ClaimNames.UserId, user.Id.ToString()),
                    new Claim(ClaimNames.Username, user.Username),
                    new Claim(ClaimTypes.Role, user.Role.Name),
                    new Claim(ClaimNames.Name, $"{user.FirstName} {user.LastName}"),
                    new Claim(JwtRegisteredClaimNames.Sub, user.Username)
                }),
                Issuer = security.Issuer,
                Audience = security.Audience,
                Expires = DateTime.Now.AddSeconds(security.ExpirationTime),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256)
            };

            var handler = new JwtSecurityTokenHandler();
            var jwtToken = handler.CreateJwtSecurityToken(tokenDesc);
            var token = handler.WriteToken(jwtToken);

            return token;
        }
    }
}