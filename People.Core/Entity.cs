﻿namespace People
{
    public class Entity<TIdentity>
        where TIdentity : struct
    {
        public TIdentity Id { get; set; }
    }

    public class Entity : Entity<int>
    { }
}
