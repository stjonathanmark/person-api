﻿using People.Dto.Schema;
using System.Linq;

namespace People.Dto
{
    public abstract class BaseRequest
    {
        [SwaggerExclude]
        public string Type
        {
            get
            {
                var attrs = (RequestTypeAttribute[])GetType().GetCustomAttributes(typeof(RequestTypeAttribute), true);
                return (attrs != null && attrs.Any() && attrs[0] != null) ? attrs[0].Name : GetType().Name.Replace("Request", string.Empty);
            }
        }

        [SwaggerExclude]
        public virtual string DefaultErrorMessage => $"An error occured executing the {Type} request.";

        [SwaggerExclude]
        public virtual string DefaultSuccessMessage => $"The {Type} request executed successfully.";
    }
}
