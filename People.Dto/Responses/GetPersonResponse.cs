﻿namespace People.Dto
{
    public class GetPersonResponse : BaseResponse
    {
        public PersonDto Person { get; set; }
    }
}
