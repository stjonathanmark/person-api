﻿using People.Dto;
using System;

namespace People.Dto
{
    [RequestType(RequestTypes.SearchPersons)]
    public class SearchPeopleRequest : BaseDataRequest
    {
        public string Prefix { get; set; }

        public string Suffix { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public DateTime? StartBirthDate { get; set; }

        public DateTime? EndBirthDate { get; set; }
    }
}
