﻿using System.Text.RegularExpressions;

namespace People.Security.Authentication
{
    public interface IPasswordSettings
    {
        bool EnforcePasswordFormat { get; set; }

        string PasswordRegexPattern { get; set; }

        Regex PasswordRegex { get; }
    }
}
