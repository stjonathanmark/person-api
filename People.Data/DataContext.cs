﻿using Microsoft.EntityFrameworkCore;
using People.Configuration;
using People.Security;
using System;

namespace People.Data
{
    public class DataContext : DbContext
    {
       

        public DataContext()
        {
        }

        public DataContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            AppConfig config = new AppConfig();
            optionsBuilder.UseSqlServer(config.Get<string>("ConnectionStrings:DataContext"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataMapper.Map(modelBuilder.Entity<Person>());
            DataMapper.Map(modelBuilder.Entity<Role>());
            DataMapper.Map(modelBuilder.Entity<User>());
        }
    }
}
