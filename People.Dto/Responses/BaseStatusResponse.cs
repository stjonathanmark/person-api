﻿using Newtonsoft.Json;
using System;

namespace People.Dto
{
    public abstract class BaseStatusResponse<TStatus> : BaseResponse
        where TStatus : Enum
    {
        [JsonProperty("statusCode")]
        public TStatus Status { get; set; }

        [JsonProperty("status")]
        public virtual string StatusName { get; set; }

        public virtual string StatusMessage { get; set; }
    }
}
