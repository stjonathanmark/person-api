﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.GetRoles)]
    public class GetRolesRequest : BaseDataRequest
    {
        public GetRolesRequest()
        {
            orderByColumn = "Name";
        }
    }
}
