﻿using System.Collections.Generic;
using People.Data;

namespace People.Dto
{
    public class BaseDataRequest : BaseRequest
    {
        protected IList<string> orderBys = null;

        protected string orderByColumn = string.Empty;

        public bool PageItems { get; set; }

        public bool IncludeDeleted { get; set; }

        public int MaxPages { get; set; } = 5;

        public int? PageSize { get; set; } = 10;

        public int? PageNumber { get; set; } = 1;

        public virtual IList<string> OrderBys
        {
            get
            {
                return orderBys ?? (string.IsNullOrEmpty(orderByColumn) ? null : OrderBy(orderByColumn, OrderByDirection.Ascending));
            }
            set { orderBys = value; }
        }

        public int? Skip => (PageItems && PageNumber.HasValue && PageSize.HasValue) ? (PageNumber - 1) * PageSize : null;

        public int? Take => PageItems ? PageSize : null;

        public IList<string> OrderBy(string name, string direction)
        {
            return new List<string>() { $"{name} {direction}" };
        }
    }
}
