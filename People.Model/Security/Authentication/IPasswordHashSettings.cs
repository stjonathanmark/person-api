﻿namespace People.Security.Authentication
{
    public interface IPasswordHashSettings
    {
        bool HashPasswords { get; set; }

        bool UseSaltedPasswords { get; set; }

        byte SaltLength { get; set; }

        string PasswordHashKey { get; set; }
    }
}
