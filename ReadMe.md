# Person REST API Demo
This is a demo Person REST API that was created with ASP.Net Core 3.1, EntityFramework Core 3.1.7, and Swagger

## Instructions to run the application in your local environment using Visual Studio 2019
1. Replace the string value being assigned to the **"ConnectionStrings:DataContext"** property on **"line 15"** in the [appsettings.json file](https://bitbucket.org/stjonathanmark/person-api/src/master/People.Web.Api/appsettings.json) in the root of the **"People.Web.Api"** project with the connection string for the test database server and the name of the database to be created using migration in next step. 
2. Make **"People.Data"** the start up project, go to the **"Package Manager Console"**, and then type and run the command **"Update-Database"** to create the database.
3. Finally make **"People.Web.Api"** the start up projects and press **"F5"** to start the application.
7. Enjoy!!!!!!!