﻿using People.Dto;

namespace People.Dto
{
    [RequestType(RequestTypes.CreateAccount)]
    public class CreateAccountRequest : BaseRequest
    {
        public int RoleId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
